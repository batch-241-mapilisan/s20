let number = Number(prompt("Please provide a number"));

for (let i = number; i >= 0; i--) {
    if (i <= 50) {
        console.log("The current value is at 50. Terminating the loop.");
        break;
    }
    if (i % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue
    } else if (i % 5 == 0) {
        console.log(i);
    }
}

let testString = "supercalifragilisticexpialidocious";
let result = ""

for (let i = 0; i < testString.length; i++) {
    if (
        testString[i] == "a" || 
        testString[i] == "e" || 
        testString[i] == "i" || 
        testString[i] == "o" || 
        testString[i] == "u" 
    ) {
        continue
    } else {
        result += testString[i]
    }
}

console.log(testString);
console.log(result);